const electron = require('electron');
const ipcRenderer = electron.ipcRenderer;
const shell = electron.shell;

const link = 'https://gitlab.com/JuniorWriter/macarena';
const macarena_win = 'D:\\Deskop\\Projects\\macarena\\installers\\Windows.bat.lnk';
const macarena_lin = 'D:\\deskop\\projects\\macarena\\installers\\Linux.sh';
const macarena_mac = 'D:\\deskop\\projects\\macarena\\installers\\macOS.applescript';

ipcRenderer.on('os', (event, data) => {
    function install(o_system){
        if (o_system == 'Windows') {
            alert('Macarena Library is about to install.')
            shell.openExternal(macarena_win);
        }
        else if (o_system == 'Linux') {
            alert('Macarena Library is about to install.')
            shell.openExternal(macarena_lin);
        }
        else if (o_system == 'macOS') {
            alert('Macarena Library is about to install.')
            shell.openExternal(macarena_mac);
        }
        else {
            document.getElementById('status').innerHTML = 'Operative system don\'t supported';
        }
    };

    function change() {
        document.getElementById('status').innerHTML = 'Successfully installed in ' + data;
    }

    function installBtn(clicks) {
        document.getElementById('install').onclick = function() {
            if (clicks >= 1) {
                shell.beep();
                alert('The library has been installed already')
            }
            else {
                change();
                install(data);    
            }
            clicks++;
        }
    }

    function opengit() {
        document.getElementById('mygit').onclick = function() {
            shell.openExternal(link);
        }
    }


    // MAIN
    var count = 0
    opengit()
    installBtn(count)
});